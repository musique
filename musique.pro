
TEMPLATE = app
CONFIG += qt

CODECFORSRC = UTF-8
CODECFORTR = ISO-8859-1

OBJECTS_DIR = build/
MOC_DIR = build/
UI_DIR = ui/


TRANSLATIONS = musique_de.ts

SOURCES += main.cpp \
 mainwindow.cpp \
 playlistmodel.cpp \
 common.cpp \
 collectionsmodel.cpp

target.path = /usr/bin
iconstarget.path = /usr/share/musique/icons
iconstarget.files = res/*.png res/musique.svg
langtarget.path = /usr/share/musique/lang
langtarget.files = musique_*.qm
shortcuttarget.path = /usr/share/applications
shortcuttarget.files = musique.desktop

INSTALLS += target iconstarget langtarget shortcuttarget

INCLUDEPATH += /usr/include/xmms2 \
 ../

LIBS += -lxmmsclient \
 -lxmmsclient-qt4

QMAKE_CXXFLAGS_DEBUG += -pedantic -Wno-long-long
TARGET = musique

DESTDIR = .

HEADERS += mainwindow.h \
constants.h \
 playlistmodel.h \
 common.h \
 collectionsmodel.h
FORMS += mainwin.ui

