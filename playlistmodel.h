//
// C++ Interface: playlistmodel
//
// Author: Oliver Groß <z.o.gross@gmx.de>, (C) 2008
//
// Copyright: See COPYING file that comes with this distribution
//
#ifndef MUSIQUE_PLAYLISTMODEL_H
#define MUSIQUE_PLAYLISTMODEL_H

#include <QAbstractItemModel>
#include <xmmsclient/xmmsclient-qt4.h>

#define MQ_PLAYLIST_STATUS   0
#define MQ_PLAYLIST_TITLE    1
#define MQ_PLAYLIST_ARTIST   2
#define MQ_PLAYLIST_DURATION 3

namespace musique {
	class CPlaylistModel : public QAbstractItemModel {
		Q_OBJECT
	public:
		CPlaylistModel(XmmsQt::QPlayback * playback, XmmsQt::QMedialib * medialib, QObject * parent = 0);
		~CPlaylistModel();
		
		XmmsQt::QPlaylist * playlist() const { return m_Playlist; };
		
		QVariant data(const QModelIndex & index, int role) const;
		Qt::ItemFlags flags(const QModelIndex & index) const;
		QVariant headerData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const;
		QModelIndex index(int row, int column, const QModelIndex & parent = QModelIndex()) const;
		QModelIndex parent(const QModelIndex & index) const;
		int rowCount(const QModelIndex & parent = QModelIndex()) const;
		int columnCount(const QModelIndex & parent = QModelIndex()) const;
	public slots:
		void setPlaylist(XmmsQt::QPlaylist * playlist);
	private:
		XmmsQt::QPlaylist * m_Playlist;
		XmmsQt::QMedialib * m_Medialib;
		XmmsQt::QPlayback * m_Playback;
		quint32 m_OldPosition;
	private slots:
		void prepareToInsertEntry(quint32 position, quint32 id);
		void insertEntry(quint32 position);
		void prepareToRemoveEntry(quint32 position, quint32 id);
		void removeEntry(quint32 position);
		void moveEntry(quint32 from, quint32 to);
		void updateEntry(quint32 position);
		void updateEntriesByID(quint32 id);
		void reloadMediaInfo();
		void updatePosition(quint32 position);
	};
}

#endif
