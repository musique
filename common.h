//
// C++ Interface: common
//
// Author: Oliver Groß <z.o.gross@gmx.de>, (C) 2008
//
// Copyright: See COPYING file that comes with this distribution
//
#include "constants.h"
#include <xmmsclient/xmmsclient-qt4.h>

class QString;

QString formatedTime(quint32 milliseconds);
QString toStringFormated(xmms_playback_status_t state);
