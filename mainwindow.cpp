//
// C++ Implementation: mainwindow
//
// Author: Oliver Groß <z.o.gross@gmx.de>, (C) 2008
//
// Copyright: See COPYING file that comes with this distribution
//
#include <QLabel>
#include <QSlider>
#include <QFileDialog>
#include <QInputDialog>
#include <QDir>
#include <QSettings>
#include <QHeaderView>
#include "mainwindow.h"
#include "common.h"
#include "playlistmodel.h"
#include "collectionsmodel.h"
#include <QDebug>

namespace musique {
	CMainWindow::CMainWindow(QWidget * parent) : QMainWindow(parent),
	m_CurrentTitle('-'), m_Client(UI_CLIENT_NAME, this),  m_Playback(&m_Client), m_Medialib(&m_Client) {
		ui.setupUi(this);
		setupToolbars();
		
		m_PlaylistModel = new CPlaylistModel(&m_Playback, &m_Medialib, this);
		ui.playlistView->setModel(m_PlaylistModel);
		ui.playlistView->addActions(ui.menuEdit->actions());
		ui.playlistView->setContextMenuPolicy(Qt::ActionsContextMenu);
		ui.playlistView->header()->setResizeMode(QHeaderView::ResizeToContents);
		connect(ui.playlistView, SIGNAL(activated(QModelIndex)), this, SLOT(switchToEntry(QModelIndex)));
		connect(ui.playlistView->selectionModel(), SIGNAL(selectionChanged(const QItemSelection &, const QItemSelection &)),
			this, SLOT(handleEntrySelectionChange(const QItemSelection &)));
		
		m_CollectionsModel = new CCollectionsModel(&m_Medialib, this);
		ui.collectionsView->setModel(m_CollectionsModel);
		connect(ui.playlistView, SIGNAL(activated(QModelIndex)), this, SLOT(switchToPlaylist(QModelIndex)));
		
		connect(&m_Playback, SIGNAL(playTimeChanged(quint32)), this, SLOT(updatePlaytime(quint32)));
		connect(&m_Playback, SIGNAL(volumeChanged()), this, SLOT(updateVolume()));
		connect(&m_Playback, SIGNAL(statusChanged(xmms_playback_status_t)), this, SLOT(updateStatus(xmms_playback_status_t)));
		connect(&m_Playback, SIGNAL(idChanged(quint32)), this, SLOT(updateCurrentId(quint32)));
		
		connect(&m_Medialib, SIGNAL(mediaInfoReady(quint32)), this, SLOT(checkMediaUpdate(quint32)));
		connect(&m_Medialib, SIGNAL(activePlaylistChanged()), this, SLOT(switchToActivePlaylist()));
		
		setupActions();
		readSettings();
		
		m_Client.connectToServer();
	}
	
	CMainWindow::~CMainWindow() {
		writeSettings();
	}
	
	void CMainWindow::readSettings() {
		QSettings settings(UI_FILE_CONFIG, QSettings::IniFormat, this);
		settings.beginGroup("Main");
		ui.actionShowCollections->setChecked(settings.value("showCollections", false).toBool());
		settings.endGroup();
		
		settings.beginGroup("MainWindow");
		resize(settings.value("size", QSize(646, 322)).toSize());
		move(settings.value("pos", QPoint(200, 200)).toPoint());
		settings.endGroup();
	}
	
	void CMainWindow::writeSettings() {
		QSettings settings(UI_FILE_CONFIG, QSettings::IniFormat, this);
		settings.beginGroup("Main");
		settings.setValue("showCollections", ui.actionShowCollections->isChecked());
		settings.endGroup();
		
		settings.beginGroup("MainWindow");
		settings.setValue("size", size());
		settings.setValue("pos", pos());
		settings.endGroup();
	}
	
	inline void CMainWindow::setupActions() {
		ui.actionPlayPause->setIcon(QIcon(UI_ICON_PLAY));
		ui.actionStop->setIcon(QIcon(UI_ICON_STOP));
		ui.actionNext->setIcon(QIcon(UI_ICON_NEXT));
		ui.actionPrevious->setIcon(QIcon(UI_ICON_PREVIOUS));
		ui.actionAddFiles->setIcon(QIcon(UI_ICON_OPEN));
		//ui.actionAddDirectory->setIcon(QIcon(UI_ICON_));
		//ui.actionAddUrl->setIcon(QIcon(UI_ICON_));
		ui.actionRemove->setIcon(QIcon(UI_ICON_REMOVE));
		ui.actionClearPlaylist->setIcon(QIcon(UI_ICON_CLEAR));
		ui.actionShufflePlaylist->setIcon(QIcon(UI_ICON_SHUFFLE));
		ui.actionShowCollections->setIcon(QIcon(UI_ICON_SHOWCOLLECTIONS));
		
		connect(ui.actionStop, SIGNAL(triggered()), &m_Playback, SLOT(stop()));
		connect(ui.actionNext, SIGNAL(triggered()), this, SLOT(switchToNext()));
		connect(ui.actionPrevious, SIGNAL(triggered()), this, SLOT(switchToPrevious()));
		connect(ui.actionAddFiles, SIGNAL(triggered()), this, SLOT(addFiles()));
		connect(ui.actionAddDirectory, SIGNAL(triggered()), this, SLOT(addDirectory()));
		connect(ui.actionAddUrl, SIGNAL(triggered()), this, SLOT(addUrl()));
		connect(ui.actionRemove, SIGNAL(triggered()), this, SLOT(removeSelectedEntries()));
		connect(ui.actionClearPlaylist, SIGNAL(triggered()), this, SLOT(clearPlaylist()));
		connect(ui.actionShufflePlaylist, SIGNAL(triggered()), this, SLOT(randomizePlaylist()));
		connect(ui.actionQuit, SIGNAL(triggered()), this, SLOT(close()));
		connect(ui.actionAboutQt, SIGNAL(triggered()), qApp, SLOT(aboutQt()));
	}
	
	inline void CMainWindow::setupToolbars() {
		m_VolumeLabel = new QLabel(this);
		m_VolumeLabel->setText(tr("Volume"));
		
		m_VolumeSlider = new QSlider(this);
		m_VolumeSlider->setMaximum(100);
		m_VolumeSlider->setOrientation(Qt::Horizontal);
		m_VolumeSlider->setSizePolicy(QSizePolicy::Preferred, QSizePolicy::Preferred);
		
		m_CurrentLabel = new QLabel(this);
		m_CurrentLabel->setText("[disconnected]");
		
/*		m_PositionSlider = new QSlider(this);
		m_PositionSlider->setEnabled(false);
		m_PositionSlider->setMaximum(1);
		m_PositionSlider->setOrientation(Qt::Horizontal);
		m_PositionSlider->setSizePolicy(QSizePolicy::Preferred, QSizePolicy::Preferred);*/
		
		m_TimeLabel = new QLabel(this);
		m_TimeLabel->setText("--:--");
		
		ui.playbackBar->addWidget(m_VolumeLabel);
		ui.playbackBar->addWidget(m_VolumeSlider);
		
		ui.currentBar->addWidget(m_CurrentLabel);
		ui.currentBar->addSeparator();
//		ui.playbackBar->addWidget(m_PositionSlider);
		ui.currentBar->addWidget(m_TimeLabel);
		
		connect(m_VolumeSlider, SIGNAL(valueChanged(int)), &m_Playback, SLOT(setVolume(int)));
		//connect(m_PositionSlider, SIGNAL(valueChanged(int)), &m_Playback, SLOT(setPlayTime(int)));
	}
	
	inline QString CMainWindow::toFormatedTitleInfo(quint32 id) {
		return m_Medialib.media()[id]->artist + " - " + m_Medialib.media()[id]->title;
	}
	
	void CMainWindow::updatePlaytime(quint32 time) {
		//TODO: check this (could be too much overhead if nothing visible changed)
		m_TimeLabel->setText(formatedTime(time));
		//m_PositionSlider->setValue(time);
	}
	
	void CMainWindow::updateVolume() {
		quint8 mid = 0;
		foreach (quint8 i, m_Playback.volume())
			mid += i;
		
		if (mid)
			mid /= m_Playback.volume().count();
		m_VolumeSlider->setValue(mid);
	}
	
	void CMainWindow::updateStatus(xmms_playback_status_t status) {
		m_CurrentLabel->setText(toStringFormated(status).arg(m_CurrentTitle));
		
		switch (status) {
		case XMMS_PLAYBACK_STATUS_STOP:
			m_TimeLabel->setText("--:--");
		case XMMS_PLAYBACK_STATUS_PAUSE:
			disconnect(ui.actionPlayPause, SIGNAL(triggered()), &m_Playback, SLOT(pause()));
			ui.actionPlayPause->setText("Play");
			ui.actionPlayPause->setIcon(QIcon(UI_ICON_PLAY));
			connect(ui.actionPlayPause, SIGNAL(triggered()), &m_Playback, SLOT(start()));
			break;
		case XMMS_PLAYBACK_STATUS_PLAY:
			disconnect(ui.actionPlayPause, SIGNAL(triggered()), &m_Playback, SLOT(start()));
			ui.actionPlayPause->setText("Pause");
			ui.actionPlayPause->setIcon(QIcon(UI_ICON_PAUSE));
			connect(ui.actionPlayPause, SIGNAL(triggered()), &m_Playback, SLOT(pause()));
			break;
		default:
			break;
		}
	}
	
	void CMainWindow::updateCurrentId(quint32 id) {
		if (id == 0)
			m_CurrentTitle = '-';
		else if (m_Medialib.checkMediaInfo(id)) {
			m_CurrentTitle = toFormatedTitleInfo(id);
			//m_PositionSlider->setMaximum(m_Medialib.media()[id]->duration);
			//m_PositionSlider->setDisabled(m_Playback.status() == XMMS_PLAYBACK_STATUS_STOP);
		}
		else {
			m_CurrentTitle = tr("loading title... (id: %1)").arg(id);
		}
		m_CurrentLabel->setText(toStringFormated(m_Playback.status()).arg(m_CurrentTitle));
	}
	
	void CMainWindow::checkMediaUpdate(quint32 id) {
		if (id == m_Playback.currentID()) {
			m_CurrentTitle = toFormatedTitleInfo(id);
			m_CurrentLabel->setText(toStringFormated(m_Playback.status()).arg(m_CurrentTitle));
		}
	}
	
	void CMainWindow::switchToNext() {
		m_Playback.setNext(m_Medialib.activePlaylist()->position()+1);
		if (m_Playback.status() != XMMS_PLAYBACK_STATUS_STOP)
			m_Playback.tickle();
	}
	
	void CMainWindow::switchToPrevious() {
		m_Playback.setNext(m_Medialib.activePlaylist()->position()-1);
		if (m_Playback.status() != XMMS_PLAYBACK_STATUS_STOP)
			m_Playback.tickle();
	}
	
	void CMainWindow::switchToEntry(QModelIndex index) {
		if (m_Medialib.activePlaylist() != m_PlaylistModel->playlist())
			m_Medialib.setActivePlaylist(m_PlaylistModel->playlist()->identifier());
		m_Playback.setNext(index.row());
		if (m_Playback.status() == XMMS_PLAYBACK_STATUS_STOP)
			m_Playback.start();
		else
			m_Playback.tickle();
	}
	
	void CMainWindow::addFiles() {
		QFileDialog fileDialog(this, tr("Select one or more files to add"), lastFilesDir);
		fileDialog.setFileMode(QFileDialog::ExistingFiles);
		if (fileDialog.exec()) {
			lastFilesDir = fileDialog.directory().path();
			foreach(QString i, fileDialog.selectedFiles()) {
				m_PlaylistModel->playlist()->append("file://" + i);
			}
		}
	}
	
	void CMainWindow::addDirectory() {
		QFileDialog directoryDialog(this, tr("Select a directory to add"), lastDirectoryDir);
		directoryDialog.setFileMode(QFileDialog::DirectoryOnly);
		if (directoryDialog.exec()) {
			lastDirectoryDir = directoryDialog.directory().path();
			m_PlaylistModel->playlist()->appendRecursive("file://" + directoryDialog.selectedFiles()[0]);
		}
	}
	
	void CMainWindow::addUrl() {
		QString url = QInputDialog::getItem(this, tr("Enter a URL to add"), "URL:", lastAppendedUrls);
		if (!url.isEmpty()) {
			lastAppendedUrls.prepend(url);
			if (lastAppendedUrls.count() > 5)
				lastAppendedUrls.removeLast();
		}
	}
	
	bool gearterRowThan(QModelIndex a, QModelIndex b) {
		return a.row() > b.row();
	}
	
	void CMainWindow::removeSelectedEntries() {
		QModelIndexList selectedIndexes = ui.playlistView->selectionModel()->selectedIndexes();
		if (selectedIndexes.isEmpty())
			return;
		
		qSort(selectedIndexes.begin(), selectedIndexes.end(), gearterRowThan);
		
		qint32 row = -1;
		foreach(QModelIndex i, selectedIndexes) {
			if (i.row() != row) {
				row = i.row();
				m_PlaylistModel->playlist()->remove(row);
			}
		}
	}
	
	void CMainWindow::clearPlaylist() {
		m_PlaylistModel->playlist()->clear();
	}
	
	void CMainWindow::randomizePlaylist() {
		m_PlaylistModel->playlist()->randomize();
	}
	
	void CMainWindow::switchToActivePlaylist() {
		//disconnect(&m_Medialib, SIGNAL(activePlaylistChanged()), this, SLOT(handleActivePlaylistOnce()));
		m_PlaylistModel->setPlaylist(m_Medialib.activePlaylist());
	}
	
	void CMainWindow::handleEntrySelectionChange(const QItemSelection & selected) {
		QModelIndexList selectedIndexes = selected.indexes();
		ui.actionRemove->setDisabled(selectedIndexes.isEmpty());
	}
	
/*	void CMainWindow::openPlaylist() {
	}
	
	void CMainWindow::savePlaylist() {
	}*/
}
