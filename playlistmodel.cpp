//
// C++ Implementation: playlistmodel
//
// Author: Oliver Groß <z.o.gross@gmx.de>, (C) 2008
//
// Copyright: See COPYING file that comes with this distribution
//
#include "playlistmodel.h"
#include "common.h"
#include <QIcon>

namespace musique {
	using namespace XmmsQt;
	
	CPlaylistModel::CPlaylistModel(QPlayback * playback, QMedialib * medialib, QObject * parent) : QAbstractItemModel(parent),
	m_Playlist(0), m_Medialib(medialib), m_Playback(playback), m_OldPosition(0) {
	}
	
	CPlaylistModel::~CPlaylistModel() {
	}
	
	void CPlaylistModel::setPlaylist(XmmsQt::QPlaylist * playlist) {
		if (m_Playlist == playlist)
			return;
		
		if (m_Playlist) {
			disconnect(m_Playlist, SIGNAL(entryAboutToBeInserted(quint32, quint32)),
			           this,       SLOT(prepareToInsertEntry(quint32, quint32)));
			disconnect(m_Playlist, SIGNAL(entryInserted(quint32)),
			           this,       SLOT(insertEntry(quint32)));
			
			disconnect(m_Playlist, SIGNAL(entryAboutToBeRemoved(quint32, quint32)),
			           this,       SLOT(prepareToRemoveEntry(quint32, quint32)));
			disconnect(m_Playlist, SIGNAL(entryRemoved(quint32)),
			           this,       SLOT(removeEntry(quint32)));
			
			disconnect(m_Playlist, SIGNAL(entryMoved(quint32, quint32)),
			           this,       SLOT(moveEntry(quint32, quint32)));
			disconnect(m_Playlist, SIGNAL(entryUpdated(quint32, quint32)),
			           this,       SLOT(updateEntry(quint32)));
			disconnect(m_Medialib, SIGNAL(mediaInfoReady(quint32)),
			           this,       SLOT(updateEntriesByID(quint32)));
			disconnect(m_Playlist, SIGNAL(reseted()),
			           this,       SLOT(reloadMediaInfo()));
			disconnect(m_Playlist, SIGNAL(positionChanged(quint32)),
			           this,       SLOT(updatePosition(quint32)));
		}
		
		m_Playlist = playlist;
		
		if (m_Playlist) {
			connect(m_Playlist, SIGNAL(entryAboutToBeInserted(quint32, quint32)),
			        this,       SLOT(prepareToInsertEntry(quint32, quint32)));
			connect(m_Playlist, SIGNAL(entryInserted(quint32)),
			        this,       SLOT(insertEntry(quint32)));
			
			connect(m_Playlist, SIGNAL(entryAboutToBeRemoved(quint32, quint32)),
			        this,       SLOT(prepareToRemoveEntry(quint32, quint32)));
			connect(m_Playlist, SIGNAL(entryRemoved(quint32)),
			        this,       SLOT(removeEntry(quint32)));
			
			connect(m_Playlist, SIGNAL(entryMoved(quint32, quint32)),
			        this,       SLOT(moveEntry(quint32, quint32)));
			connect(m_Playlist, SIGNAL(entryUpdated(quint32, quint32)),
			        this,       SLOT(updateEntry(quint32)));
			connect(m_Medialib, SIGNAL(mediaInfoReady(quint32)),
			        this,       SLOT(updateEntriesByID(quint32)));
			connect(m_Playlist, SIGNAL(reseted()),
			        this,       SLOT(reloadMediaInfo()));
			
			connect(m_Playlist, SIGNAL(positionChanged(quint32)),
			           this,       SLOT(updatePosition(quint32)));
			
			reloadMediaInfo();
		}
		else
			emit layoutChanged();
	}
	
	void CPlaylistModel::prepareToInsertEntry(quint32 position, quint32 id) {
		beginInsertRows(QModelIndex(), position, position);
		//m_Medialib->checkMediaInfo(id);
	}
	
	void CPlaylistModel::insertEntry(quint32 position) {
		endInsertRows();
	}
	
	void CPlaylistModel::prepareToRemoveEntry(quint32 position, quint32 id) {
		beginRemoveRows(QModelIndex(), position, position);
		//m_Medialib->removeMediaInfo(id);
	}
	
	void CPlaylistModel::removeEntry(quint32 position) {
		endRemoveRows();
	}
	
	void CPlaylistModel::moveEntry(quint32 from, quint32 to) {
		emit dataChanged(index(from, 0), index(to, columnCount()));
	}
	
	void CPlaylistModel::updateEntry(quint32 position) {
		emit dataChanged(index(position, 0), index(position, columnCount()));
		quint32 id = m_Playlist->idList()[position];
		m_Medialib->checkMediaInfo(id);
	}
	
	void CPlaylistModel::updatePosition(quint32 position) {
		emit dataChanged(index(m_OldPosition, 0), index(m_OldPosition, columnCount()));
		m_OldPosition = position;
		emit dataChanged(index(m_OldPosition, 0), index(m_OldPosition, columnCount()));
	}
	
	void CPlaylistModel::updateEntriesByID(quint32 id) {
		for(quint32 i = 0; i < m_Playlist->idList().count(); i++){
			if (m_Playlist->idList()[i] == id)
				emit dataChanged(index(i, 0), index(i, columnCount()));
		}
	}
	
	void CPlaylistModel::reloadMediaInfo() {
		foreach(quint32 i, m_Playlist->idList()) {
			m_Medialib->checkMediaInfo(i);
		}
		emit layoutChanged();
	}
	
	QVariant CPlaylistModel::data(const QModelIndex & index, int role) const {
		if (!index.isValid())
			return QVariant();
		
		if ((role == Qt::DecorationRole) && (index.column() == MQ_PLAYLIST_STATUS) &&
			(index.row() == m_Playlist->position())) {
/*			if ((m_Playlist == m_Medialib->activePlaylist()) && (index.internalId() == m_Playback->currentID()))
				return QIcon(UI_ICON_NOWPLAYING);
			else*/
				return QIcon(UI_ICON_POSITION);
		}
		
		if (role != Qt::DisplayRole)
			return QVariant();
		
		quint32 id = index.internalId();
		
		if (m_Medialib->media().contains(id)) {
			MediaInfo * data = m_Medialib->media()[id];
			switch (index.column()) {
			case MQ_PLAYLIST_TITLE:
				return data->title;
			case MQ_PLAYLIST_ARTIST:
				return data->artist;
			case MQ_PLAYLIST_DURATION:
				return formatedTime(data->duration);
			default:
				break;
			}
		}
		else {
			switch (index.column()) {
			case MQ_PLAYLIST_TITLE:
				return tr("unknown (id: %1)").arg(id);
			case MQ_PLAYLIST_ARTIST:
				return tr("unknown");
			case MQ_PLAYLIST_DURATION:
				return QString("--:--");
			default:
				break;
			}
		}
		
		return QVariant();
	}
	
	Qt::ItemFlags CPlaylistModel::flags(const QModelIndex & index) const {
		if (!index.isValid())
			return 0;
		
		return Qt::ItemIsEnabled | Qt::ItemIsSelectable;
	}
	
	QVariant CPlaylistModel::headerData(int section, Qt::Orientation orientation, int role) const {
		if (role != Qt::DisplayRole)
			return QVariant();
		
		if (orientation == Qt::Horizontal) {
			switch (section) {
			case MQ_PLAYLIST_TITLE:
				return tr("Title");
			case MQ_PLAYLIST_ARTIST:
				return tr("Artist");
			case MQ_PLAYLIST_DURATION:
				return tr("Time");
			default:
				break;
			}
		}
		return QVariant();
	}
	
	QModelIndex CPlaylistModel::index(int row, int column, const QModelIndex & parent) const {
		if (!m_Playlist || parent.isValid() || m_Playlist->idList().isEmpty() || row >= m_Playlist->idList().count())
			return QModelIndex();
		else
			return createIndex(row, column, m_Playlist->idList()[row]);
	}
	
	QModelIndex CPlaylistModel::parent(const QModelIndex & index) const {
		return QModelIndex();
	}
	
	int CPlaylistModel::rowCount(const QModelIndex & parent) const {
		if (!m_Playlist || parent.isValid())
			return 0;
		else
			return m_Playlist->idList().count();
	}
	
	int CPlaylistModel::columnCount(const QModelIndex & parent) const {
		return 4;
	}
}
