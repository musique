//
// C++ Implementation: playlistlistmodel
//
// Author: Oliver Groß <z.o.gross@gmx.de>, (C) 2008
//
// Copyright: See COPYING file that comes with this distribution
//
#include "collectionsmodel.h"

namespace musique {
	using namespace XmmsQt;
	
	CCollectionsModel::CCollectionsModel(XmmsQt::QMedialib * medialib, QObject * parent) : QAbstractListModel(parent),
	m_Medialib(medialib) {
		if (m_Medialib)
			connect(m_Medialib, SIGNAL(playlistsReseted()), this, SIGNAL(layoutChanged()));
	}
	
	CCollectionsModel::~CCollectionsModel() {
	}
	
	QVariant CCollectionsModel::data(const QModelIndex & index, int role) const {
		if (role == Qt::DisplayRole) {
			QPlaylist * playlist = static_cast<QPlaylist *>(index.internalPointer());
			if (playlist == m_Medialib->activePlaylist())
				return playlist->identifier() + ' ' + tr("[active]");
			else
				return playlist->identifier();
		}
		else
			return QVariant();
	}
	
	QVariant CCollectionsModel::headerData(int section, Qt::Orientation orientation, int role) const {
		return tr("Playlists");
	}
	
	QModelIndex CCollectionsModel::index(int row, int column, const QModelIndex & parent) const {
		if (m_Medialib && !m_Medialib->playlists().isEmpty())
			return createIndex(row, column, m_Medialib->playlists()[m_Medialib->playlists().keys()[row]]);
		else
			return QModelIndex();
	}
	
	int CCollectionsModel::rowCount(const QModelIndex & parent) const {
		if (m_Medialib && (parent == QModelIndex()))
			return m_Medialib->playlists().count();
		else
			return 0;
	}
}
