//
// C++ Interface: playlistlistmodel
//
// Author: Oliver Groß <z.o.gross@gmx.de>, (C) 2008
//
// Copyright: See COPYING file that comes with this distribution
//
#ifndef MUSIQUE_PLAYLISTLISTMODEL_H
#define MUSIQUE_PLAYLISTLISTMODEL_H

#include <QAbstractListModel>
#include <xmmsclient/xmmsclient-qt4.h>

namespace musique {
	class CCollectionsModel : public QAbstractListModel {
		Q_OBJECT
	public:
		CCollectionsModel(XmmsQt::QMedialib * medialib, QObject * parent = 0);
		~CCollectionsModel();
		
		QVariant data(const QModelIndex & index, int role) const;
		QVariant headerData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const;
		QModelIndex index(int row, int column, const QModelIndex & parent = QModelIndex()) const;
		int rowCount(const QModelIndex & parent = QModelIndex()) const;
	private:
		XmmsQt::QMedialib * m_Medialib;
	};
}

#endif
