#ifndef MUSIQUE_CONSTANTS_H
#define MUSIQUE_CONSTANTS_H

#define UI_VERSION                      "0.1"
#define UI_NAME                         tr("Musique - Qt XMMS2 client")
#define UI_CLIENT_NAME                  "musique"

#define UI_DIR_WORK                     ".musique/"

#define UI_PATH_TRANSLATIONS            "/usr/share/musique/lang/"
//#define UI_PATH_ICONS                   "res/"
#define UI_PATH_ICONS                   "/usr/share/musique/icons/"
#define UI_PATH_WORK                    (QDir::toNativeSeparators(QDir::homePath()) + "/" UI_DIR_WORK)

#define UI_FILE_CONFIG                  (UI_PATH_WORK + "musique.conf")

#define UI_ICON_MUSIQUE                 UI_PATH_ICONS "musique.svg"

#define UI_ICON_PLAY                    UI_PATH_ICONS "play.png"
#define UI_ICON_PAUSE                   UI_PATH_ICONS "pause.png"
#define UI_ICON_NEXT                    UI_PATH_ICONS "next.png"
#define UI_ICON_PREVIOUS                UI_PATH_ICONS "previous.png"
#define UI_ICON_STOP                    UI_PATH_ICONS "stop.png"

#define UI_ICON_EJECT                   UI_PATH_ICONS "eject.png"

#define UI_ICON_NEW                     UI_PATH_ICONS "new.png"
#define UI_ICON_REMOVE                  UI_PATH_ICONS "delete.png"
#define UI_ICON_CLEAR                   UI_PATH_ICONS "clear.png"
#define UI_ICON_OPEN                    UI_PATH_ICONS "open.png"
#define UI_ICON_SAVEAS                  UI_PATH_ICONS "saveas.png"

#define UI_ICON_SHUFFLE                 UI_PATH_ICONS "shuffle.png"

#define UI_ICON_NOWPLAYING              UI_PATH_ICONS "nowplaying.png"
#define UI_ICON_POSITION                UI_PATH_ICONS "position.png"

#define UI_ICON_SHOWCOLLECTIONS         UI_PATH_ICONS "showcollections.png"

#define UI_ICON_EXIT                    UI_PATH_ICONS "exit.png"

#endif
