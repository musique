//
// C++ Implementation: common
//
// Author: Oliver Groß <z.o.gross@gmx.de>, (C) 2008
//
// Copyright: See COPYING file that comes with this distribution
//
#include <QString>
#include "common.h"

QString toStringFormated(xmms_playback_status_t state) {
	switch (state) {
	case XMMS_PLAYBACK_STATUS_PLAY:
		return "[PLAYING] %1";
	case XMMS_PLAYBACK_STATUS_STOP:
		return "[STOPPED] %1";
	case XMMS_PLAYBACK_STATUS_PAUSE:
		return "[PAUSED] %1";
	default:
		return "%1";
	}
}

QString formatedTime(quint32 milliseconds) {
	quint16 seconds = milliseconds / 1000 + (milliseconds % 1000 > 500 ? 1 : 0);
	quint8 minutes = seconds / 60;
	quint8 hours   = minutes / 60;
	
	QString result;
	
	if (hours) {
		if (hours < 10)
			result += '0';
		result += QString::number(hours);
		result += ':';
	}
	
	minutes %= 60;
	
	if (minutes < 10)
		result += '0';
	result += QString::number(minutes);
	result += ':';
	
	seconds %= 60;
	
	if (seconds < 10)
		result += '0';
	result += QString::number(seconds);
	
	return result;
}
