//
// C++ Interface: mainwindow
//
// Author: Oliver Groß <z.o.gross@gmx.de>, (C) 2008
//
// Copyright: See COPYING file that comes with this distribution
//
#ifndef MUSIQUE_MAINWINDOW_H
#define MUSIQUE_MAINWINDOW_H

#include <QMainWindow>
#include <xmmsclient/xmmsclient-qt4.h>
#include "ui/ui_mainwin.h"

class QLabel;
class QSlider;

namespace musique {
	class CPlaylistModel;
	class CCollectionsModel;
	
	class CMainWindow : public QMainWindow {
		Q_OBJECT
	public:
		CMainWindow(QWidget * parent = 0);
		~CMainWindow();
	public slots:
		void switchToNext();
		void switchToPrevious();
		//void openFiles();
		void addFiles();
		void addDirectory();
		void addUrl();
		void removeSelectedEntries();
		void clearPlaylist();
		void randomizePlaylist();
	protected:
		QString lastFilesDir;
		QString lastDirectoryDir;
		QStringList lastAppendedUrls;
		
		Ui::mainWindow ui;
		QSlider * m_VolumeSlider;
		QSlider * m_PositionSlider;
		QLabel * m_VolumeLabel;
		QLabel * m_CurrentLabel;
		QLabel * m_TimeLabel;
		
		QString m_CurrentTitle;
		
		CPlaylistModel * m_PlaylistModel;
		CCollectionsModel * m_CollectionsModel;
		
		XmmsQt::QClient m_Client;
		XmmsQt::QPlayback m_Playback;
		XmmsQt::QMedialib m_Medialib;
		
		void readSettings();
		void writeSettings();
		inline void setupActions();
		inline void setupToolbars();
		inline QString toFormatedTitleInfo(quint32 id);
	protected slots:
		void updatePlaytime(quint32 time);
		void updateVolume();
		void updateStatus(xmms_playback_status_t status);
		void updateCurrentId(quint32 id);
		void checkMediaUpdate(quint32 id);
		void switchToEntry(QModelIndex index);
		//void switchToPlaylist(QModelIndex index);
		void switchToActivePlaylist();
		//void handleActivePlaylistOnce();
		void handleEntrySelectionChange(const QItemSelection & selected);
	};
}

#endif
